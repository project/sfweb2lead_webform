<?php

namespace Drupal\sfweb2lead_webform\Event;

use Drupal\sfweb2lead_webform\Plugin\WebformHandler\SalesforceWebToLeadPostWebformHandler;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Sfweb2lead webform event.
 */
class Sfweb2leadWebformEvent extends Event {

  const SUBMIT = 'sfweb2lead_webform.submit';

  /**
   * Webform data.
   *
   * @var array
   */
  protected $data;

  /**
   * The sfweb2lead webform handler.
   *
   * @var \Drupal\sfweb2lead_webform\Plugin\WebformHandler\SalesforceWebToLeadPostWebformHandler
   */
  protected $handler;

  /**
   * Webform submission object.
   *
   * @var \Drupal\webform\WebformSubmissionInterface
   */
  protected $submission;

  /**
   * Sfweb2leadWebformEvent constructor.
   *
   * @param array $data
   *   An array containing webform data.
   * @param \Drupal\sfweb2lead_webform\Plugin\WebformHandler\SalesforceWebToLeadPostWebformHandler $handler
   *   The sfweb2lead webform handler.
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   The webform submission object.
   */
  public function __construct(array $data, SalesforceWebToLeadPostWebformHandler $handler, WebformSubmissionInterface $submission) {
    $this->data = $data;
    $this->handler = $handler;
    $this->submission = $submission;
  }

  /**
   * Data getter.
   *
   * @return array
   *   An array containing webform data.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Data setter.
   *
   * @param array $data
   *   Data.
   *
   * @return $this
   */
  public function setData(array $data) {
    $this->data = $data;
    return $this;
  }

  /**
   * Get the sfweb2lead_webform webform handler.
   *
   * @return \Drupal\sfweb2lead_webform\Plugin\WebformHandler\SalesforceWebToLeadPostWebformHandler
   *   The sfweb2lead webform handler.
   */
  public function getHandler() {
    return $this->handler;
  }

  /**
   * Get webform submission.
   *
   * @return \Drupal\webform\WebformSubmissionInterface
   *   The webform submission object.
   */
  public function getSubmission() {
    return $this->submission;
  }

}
